package de.favo.exercisecalculator;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 *     Exercise Calculator
 *     Copyright (C) 2016  Felix von Oertzen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Exercise extends AppCompatActivity {
    protected static final String EXTRA_FIRST_RANGE_BEGIN = "rangeStart1";
    protected static final String EXTRA_SECOND_RANGE_BEGIN = "rangeStart2";
    protected static final String EXTRA_FIRST_RANGE_END = "rangeEnd1";
    protected static final String EXTRA_SECOND_RANGE_END = "rangeEnd2";
    protected static final String EXTRA_AMOUNT = "amount";
    protected static final String EXTRA_OPERATION = "operation";
    private String textFiled = "";
    @SuppressWarnings("FieldCanBeLocal")
    private Button[] numbers;
    @SuppressWarnings("FieldCanBeLocal")
    private Button minus;
    @SuppressWarnings("FieldCanBeLocal")
    private ImageButton delete;
    @SuppressWarnings("FieldCanBeLocal")
    private ImageButton back;
    @SuppressWarnings("FieldCanBeLocal")
    private ImageButton forward;
    private TextView task, answer, title;
    private CalculationManager calculationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);
        Intent in = getIntent();


        back = findViewById(R.id.previous);
        forward = findViewById(R.id.next);
        task = findViewById(R.id.exercise_task);
        answer = findViewById(R.id.exercise_answer);
        title = findViewById(R.id.title_exercise);

        numbers = new Button[10];

        numbers[0] = findViewById(R.id.exercise_0);
        numbers[1] = findViewById(R.id.exercise_1);
        numbers[2] = findViewById(R.id.exercise_2);
        numbers[3] = findViewById(R.id.exercise_3);
        numbers[4] = findViewById(R.id.exercise_4);
        numbers[5] = findViewById(R.id.exercise_5);
        numbers[6] = findViewById(R.id.exercise_6);
        numbers[7] = findViewById(R.id.exercise_7);
        numbers[8] = findViewById(R.id.exercise_8);
        numbers[9] = findViewById(R.id.exercise_9);
        minus = findViewById(R.id.exercise__);
        delete = findViewById(R.id.exercise_x);

        if (savedInstanceState != null) {
            calculationManager = (CalculationManager) savedInstanceState.getSerializable("CalculationManager");
            textFiled = savedInstanceState.getString("answer");
            task.setText(Html.fromHtml(savedInstanceState.getString("task")));
            answer.setText(textFiled);
        } else {
            (new CalculationGenerator()).execute(
                    in.getIntExtra(EXTRA_OPERATION, 0),
                    in.getIntExtra(EXTRA_AMOUNT, 0),
                    in.getIntExtra(EXTRA_FIRST_RANGE_BEGIN, 0),
                    in.getIntExtra(EXTRA_SECOND_RANGE_BEGIN, 0),
                    in.getIntExtra(EXTRA_FIRST_RANGE_END, 0),
                    in.getIntExtra(EXTRA_SECOND_RANGE_END, 0));
        }

        for (int i = 0; i < 10; i++) {
            final int finalI = i;
            numbers[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addText("" + finalI);
                }
            });
        }

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteText();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Exercise.this, Calculator.class));
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeValue();
            }
        });
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (endUp()) {
                    if (calculationManager.hasNext())
                        setUp(calculationManager.next());
                    else {
                        Intent i = new Intent(Exercise.this, ResultActivity.class);
                        i.putExtra(ResultActivity.EXTRA_MGR, calculationManager);
                        startActivity(i);
                    }
                } else {
                    Toast.makeText(Exercise.this, R.string.text_nothing_entered, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void setUp(Calculation c) {
        answer.setText("");
        textFiled = "";
        task.setText(Html.fromHtml(c.getCalculation()));
        title.setText(getString(R.string.text_exercise,
                calculationManager.getPosition() + 1, calculationManager.getSize()));
    }

    private boolean endUp() {
        if (calculationManager.getPosition() != -1)
            if (!answer.getText().toString().isEmpty()) {
                calculationManager.elementAt(
                        calculationManager.getPosition())
                        .setPutativeResult(Integer.parseInt(answer.getText().toString()));
                return true;
            } else
                return false;
        return false;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("CalculationManager", calculationManager);
        outState.putString("answer", textFiled);
        outState.putString("task", calculationManager.current().getCalculation());
    }

    private void addText(String t) {
        textFiled = textFiled + t;
        answer.setText(textFiled);
    }

    private void changeValue() {
        if (textFiled.startsWith("-"))
            textFiled = textFiled.substring(1);
        else
            textFiled = "-".concat(textFiled);
        answer.setText(textFiled);
    }

    private void deleteText() {
        if (!textFiled.isEmpty()) {
            textFiled = textFiled.substring(0, textFiled.length() - 1);
            answer.setText(textFiled);
        }
    }

    @SuppressLint("StaticFieldLeak")
    class CalculationGenerator extends AsyncTask<Integer, Integer, CalculationManager> {
        ProgressDialog progressdialog;
        @Override
        protected CalculationManager doInBackground(Integer... params) {
            progressdialog.setMax(params[1]);
            return new CalculationManager(
                    params[0],
                    params[1],
                    params[2],
                    params[3],
                    params[4],
                    params[5], new CalculationManager.State() {
                @Override
                public void onProgressChanged(int state, int target) {
                    onProgressUpdate(state, target);
                }
            });
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressdialog = new ProgressDialog(Exercise.this);
            progressdialog.setMessage(getString(R.string.text_generating_tasks));
            progressdialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressdialog.setIndeterminate(false);
            progressdialog.setCanceledOnTouchOutside(false);
            progressdialog.show();
        }

        @Override
        protected void onPostExecute(CalculationManager cm) {
            progressdialog.dismiss();
            calculationManager = cm;
            if (calculationManager.hasNext())
                setUp(calculationManager.next());
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            progressdialog.setProgress(values[0]);
        }


    }
}
