package de.favo.exercisecalculator;

import java.util.Random;

/**
 *     Exercise Calculator
 *     Copyright (C) 2016  Felix von Oertzen
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class RandomManager {
    private Random r;
    private int min,max;
    RandomManager(int min, int max){
        r = new Random();
        this.min = min;
        this.max = max;
        this.max += 1;
    }
    public int getInt() {
        if (min == max-1)
            return min;
        return min + r.nextInt(max-min);

    }
}
